# Сортировка файлов слиянием

## Used Environment

Java 1.8

## Сборка с зависимостями

mvn clean compile assembly:single

## CLI:

```
<dependency>
            <groupId>commons-cli</groupId>
			<artifactId>commons-cli</artifactId>
			<version>1.4</version>
</dependency>
```

## Опции

1 (необязательная): -d нисходящая сортировка, -a восходящая. 
	                   По умолчанию восходящая
2 (обязательная): -i входные данные целочисленные или
	              -s входные данные строковые
				  
	
## Аргументы

1 Выходной файл .txt. Если не существует,
  то будет создан в корневой директории.
2 Входные один или более файлы. Должны находиться 
  в корневой директории.
  
  
  
## Примеры запуска:

	       testproject-1.0-jar-with-dependencies.jar -s out.txt in1.txt in2.txt in3.txt
	       testproject-1.0-jar-with-dependencies.jar -d -i out.txt in1.txt in2.txt in3.txt
