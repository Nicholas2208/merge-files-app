package mergefiles.merge;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import mergefiles.cmdline.AppSettings;
import mergefiles.cmdline.ContentType;
import mergefiles.cmdline.SortMode;
import mergefiles.deserializer.IntDeserializer;

import static org.junit.Assert.*;

public class FilesMergerTest {
	private AppSettings settings;
	private List<File> files;
    File inputFile1;
    File inputFile2;
    File inputFile3;
    File outputFile;
    
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    
    @Before
    public void setUp() {
    	try {
    		files = new ArrayList<>();
    		inputFile1 = folder.newFile( "in1.txt" );
    		inputFile2 = folder.newFile( "in2.txt" );
    		inputFile3 = folder.newFile( "in3.txt" );
            outputFile = folder.newFile("out.txt");
    	} catch( IOException e ) {
            System.err.println(e.getMessage());
        } 
    }
    
    @Test
    public void whenSortStringsDesc() throws IOException {
    	try {
    		Files.write(inputFile1.toPath(), Arrays.asList("vevay", "sharona", "roscoe", "ronna", "mattie", "kerk",
    				"kania", "jobie", "jessy", "jerald", "genvieve", "elyse",
    				"diarmid", "olga1", "conrad", "any"));
    		Files.write(inputFile2.toPath(), Arrays.asList("sonnnie", "sari", "robinetta", "lin", "jordana", "jasmin", "axel", "austina",
    				"nicholas", "albertina", "alayne"));
    		Files.write(inputFile3.toPath(), Arrays.asList("udell", "tamqrah", "syd", "rena", "meara", "lucien", 
    				"katti", "glynnis", "erick", "dolley", "ggg201", "astra"));
        }
        catch(IOException e ) {
            System.err.println(e.getMessage());
        }
    	
    	files.add(inputFile1);
        files.add(inputFile2);
        files.add(inputFile3);
        
        settings = new AppSettings(outputFile, files, ContentType.STRING, SortMode.DESCEND);
        FilesMerger<String> filesMerger = new FilesMerger<>(settings, s -> s);
        filesMerger.mergeSortedFiles();
        
        File expected = new File("./src/main/resources/expected/stringssSortDesc.txt");
        assertEquals(FileUtils.readLines(expected, "UTF-8"), FileUtils.readLines(outputFile, "UTF-8"));
    }
    
    @Test
    public void whenSortNumbersDesc() throws IOException {
    	try {
            Files.write(inputFile1.toPath(), Arrays.asList("10001", "333", "12", "200", "-50"));
            Files.write(inputFile2.toPath(), Arrays.asList("1000000"));
            Files.write(inputFile3.toPath(), Arrays.asList("555", "abc", "300", "0", "-22", "-100"));
        }
        catch(IOException e ) {
            System.err.println(e.getMessage());
        }
    	
    	files.add(inputFile1);
        files.add(inputFile2);
        files.add(inputFile3);
        
        settings = new AppSettings(outputFile, files, ContentType.INTEGER, SortMode.DESCEND);
        FilesMerger<Integer> filesMerger = new FilesMerger<>(settings, new IntDeserializer());
        filesMerger.mergeSortedFiles();
        
        File expected = new File("./src/main/resources/expected/numbersSortDesc.txt");
        assertEquals(FileUtils.readLines(expected, "UTF-8"), FileUtils.readLines(outputFile, "UTF-8"));
    }
    
    @Test
    public void whenSortNumbersAsc() throws IOException {
    	try {
            Files.write(inputFile1.toPath(), Arrays.asList("2", "5", "100", "###", "222", "1001"));
            Files.write(inputFile2.toPath(), Arrays.asList("-20","50", "110", "abs", "20000"));
            Files.write(inputFile3.toPath(), Arrays.asList("0", "12", "111"));
        }
        catch(IOException e ) {
            System.err.println(e.getMessage());
        }
    	
    	files.add(inputFile1);
        files.add(inputFile2);
        files.add(inputFile3);
        
        settings = new AppSettings(outputFile, files, ContentType.INTEGER, SortMode.ASCEND);
        FilesMerger<Integer> filesMerger = new FilesMerger<>(settings, new IntDeserializer());
        filesMerger.mergeSortedFiles();
        
        File expected = new File("./src/main/resources/expected/numbersSortAsc.txt");
        assertEquals(FileUtils.readLines(expected, "UTF-8"), FileUtils.readLines(outputFile, "UTF-8"));
    }
    
    @Test
    public void whenSortStringssAsc() throws IOException {
    	try {
    		Files.write(inputFile1.toPath(), Arrays.asList("any", "conrad", "diarmid", "###", "elyse", "genvieve",
    				"jerald", "jessy", "jobie", "kania", "kerk", "mattie", "ronna",
    				"roscoe", "nicholas", "sharona", "vevay"));
    		Files.write(inputFile2.toPath(), Arrays.asList("alayne", "albertina", "110", "austina", "axel", "jasmin", "jordana", "lin",
    				"@@@@", "robinetta", "sari", "sonnnie"));
    		Files.write(inputFile3.toPath(), Arrays.asList("astra", "dolley", "224", "erick", "glynnis", "katti", 
    				"lucien", "meara", "rena", "syd", "olga1", "tamqrah", "udell"));
        }
        catch(IOException e ) {
            System.err.println(e.getMessage());
        }
    	
    	files.add(inputFile1);
        files.add(inputFile2);
        files.add(inputFile3);
        
        settings = new AppSettings(outputFile, files, ContentType.STRING, SortMode.ASCEND);
        FilesMerger<String> filesMerger = new FilesMerger<>(settings, s -> s);
        filesMerger.mergeSortedFiles();
        
        File expected = new File("./src/main/resources/expected/stringssSortAsc.txt");
        assertEquals(FileUtils.readLines(expected, "UTF-8"), FileUtils.readLines(outputFile, "UTF-8"));
    }

}
