package mergefiles;

import mergefiles.cmdline.AppSettings;
import mergefiles.cmdline.CmdLineArgumentsParser;
import mergefiles.cmdline.ContentType;
import mergefiles.deserializer.IntDeserializer;
import mergefiles.merge.FilesMerger;

public class CommandLineApp {

	public static void main(String[] args) {
		AppSettings settings = CmdLineArgumentsParser.parse(args);
		
		if (settings.getContentType() == ContentType.INTEGER) {
			FilesMerger<Integer> filesMerger = new FilesMerger<>(settings, new IntDeserializer());
			filesMerger.mergeSortedFiles();
		} else if (settings.getContentType() == ContentType.STRING){
			FilesMerger<String> filesMerger = new FilesMerger<>(settings, s -> s);
			filesMerger.mergeSortedFiles();
		}
	}
}
