package mergefiles.cmdline;

public enum SortMode {
	ASCEND,
    DESCEND
}
