package mergefiles.cmdline;

import java.io.File;
import java.util.List;

public class AppSettings {
	private final File output;
	private final List<File> input;
	private final ContentType contentType;
    private final SortMode sortMode;
    
    public AppSettings(File out, List<File> in, ContentType ct, SortMode sm) {
    	this.output = out;
    	this.input = in;
        this.contentType = ct;
        this.sortMode = sm;
    }
    
    public List<File> getInput() { return input; }
    public File getOutput() { return output; }
    public ContentType getContentType() { return contentType; }
    public SortMode getSortMode() { return sortMode; }
}
