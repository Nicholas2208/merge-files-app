package mergefiles.cmdline;

import java.io.File;
import java.util.List;

public class AppSettingsBuilder {
	private File output;
	private List<File> input;
    private ContentType contentType;
    private SortMode sortMode;
    
    public File getOutput() { return output; }
    public List<File> getInput() { return input; }
    public ContentType getContentType() { return contentType; }
    public SortMode getSortMode() { return sortMode; }
    
    public void setOutput(File output) { this.output = output; }
    public void setInput(List<File> input) { this.input = input; }
    public void setContentType(ContentType ct) { this.contentType = ct; }
    public void setSortMode(SortMode sm) { this.sortMode = sm; }
    
    public AppSettings build() {
        return new AppSettings(output, input, contentType, sortMode);
    }
}
