package mergefiles.cmdline;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CmdLineArgumentsParser {
	
	public static AppSettings parse(String[] args) throws IllegalArgumentException {
		if (args.length < 1) {
			System.out.println("\nВызов справки: -h или --help\n");
		}
		
		CommandLineParser parser = new DefaultParser();
		CommandLine line = null;
		
		try {
			line = parser.parse(buildOptions(), args);
			
		} catch (ParseException ex) {
            System.err.println("Error: " + ex.getMessage());
            System.exit(1);
        }
		
		if (line.hasOption('h')) {
            printHelp();
            System.exit(0);
        }
		
		if (line.getArgs().length < 2) {
			System.err.println("Необходимо ввести имя выходного фала, " +
		       "и имена одного или более входных фалов.\n\n");
			printHelp();
			System.exit(0);
		}
		
		String output = line.getArgs()[0];
		
		File[] inputFiles = new File[line.getArgs().length-1];
		for (int i = 1; i < line.getArgs().length; i++) {
			String name = line.getArgs()[i];
			inputFiles[i-1] = new File(name);
		}
		
		AppSettingsBuilder bAppSettings = new AppSettingsBuilder();
		bAppSettings.setOutput(new File(output));
		bAppSettings.setInput(Arrays.asList(inputFiles));
		
		if (line.hasOption('i')) {
			bAppSettings.setContentType(ContentType.INTEGER);
		} else if (line.hasOption('s')) {
			bAppSettings.setContentType(ContentType.STRING);
		} else {
			System.err.printf("Тип данных не поддерживается \n");
            throw new IllegalArgumentException();
		}
		
		if (line.hasOption('d')) {
        	bAppSettings.setSortMode(SortMode.DESCEND);
        } else {
        	bAppSettings.setSortMode(SortMode.ASCEND);
        }
		
		return bAppSettings.build();
	}

	public static Options buildOptions() {
		Options options = new Options();
		
		options.addOption("h", "help", false, "Вывод справки.");
		options.addOption("a", "ascend", false, "Восходящий режим сортировки.");
		options.addOption("d", "descend", false, "Нисходящий режим сортировки.");
		options.addOption("s", "string", false, "Тип данных - строки.");
		options.addOption("i", "integer", false, "Тип данных - целые числа.");
		
		return options;
	}
	
	private static void printHelp() {
		System.out.println("Введите аргументы в следующем порядке:\n");
		System.out.println("1). Имя выходного файла (обязательно)");
		System.out.println("2). Далее (не менее одного) имена входных фалов\n");
		System.out.println("Флажки: ");
		System.out.println("1). Тип данных (обязательно): -s (--string) или -i (--integer)");
		System.out.println("2). Режим сортировки (не обязательно): -a (--ascend) восходящий, " 
		    + "\n\t\tили -d (--descend) нисходящий. По умолчанию восходящий");
		System.out.println("\nПримеры запуска в коммандной строке(Windows):");
		System.out.println("java -jar MergeFiles.jar -s out.txt in1.txt in2.txt in3.txt");
		System.out.println("java -jar MergeFiles.jar -i -d out.txt in1.txt in2.txt");
		System.out.println("java -jar MergeFiles.jar -d -s out.txt in1.txt in2.txt in3.txt");
	}
}
