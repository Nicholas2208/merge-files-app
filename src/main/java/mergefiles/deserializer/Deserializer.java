package mergefiles.deserializer;

public interface Deserializer<T> {
	T deserialize(String s);
}
