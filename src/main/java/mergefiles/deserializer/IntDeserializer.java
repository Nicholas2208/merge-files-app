package mergefiles.deserializer;

public class IntDeserializer implements Deserializer<Integer> {

	@Override
	public Integer deserialize(String s) throws NumberFormatException{
		if (isInteger(s)) {
			 return Integer.valueOf(s);
		}
		throw new NumberFormatException();
	}

	private boolean isInteger(String str){
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
