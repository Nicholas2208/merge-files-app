package mergefiles.comparators;

public interface SortingComparator<T> {
	boolean compare(T a, T b);
}
