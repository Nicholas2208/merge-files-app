package mergefiles.merge;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

import mergefiles.comparators.SortingComparator;
import mergefiles.deserializer.Deserializer;

public class FileScannerPair<T extends Comparable<T>> {
	private final String fileName;
    private Scanner scanner;
    private T previous = null;
    private T current = null;
    private Deserializer<T> deserializer;
    private SortingComparator<T> comparator;
    private boolean eof = false;
    
    public FileScannerPair(String fileName, Deserializer<T> deserializer, SortingComparator<T> comparator) {
    	this.deserializer = deserializer;
   	    this.fileName = fileName;
   	    this.comparator = comparator;
   	    try {
		    scanner = new Scanner(new FileInputStream(fileName));
	    } catch (FileNotFoundException e) {
         System.out.println(e);
        }
   	 
   	    current = getNextItem();
    }

    private T getNextItem() {
    	if (scanner.hasNext()) {
    		String nextLine = scanner.nextLine();
    		
    		if (nextLine.contains(" ") || nextLine.isEmpty()) {
    			System.err.printf("\nФайл: %s. Строка %s содержит пробел(ы) или пустая."
    					+ " Она будет пропущена.", fileName, nextLine);
    			
    			return null;
    		}
    		
    		try {
    			T val = deserializer.deserialize(nextLine);
    			return val;
    		} catch (NumberFormatException e) {
    			System.err.printf("\nФайл: %s. Строку %s не удалось конвертировать в число."
    					+ " Она будет пропущена.", fileName, nextLine);
    			return null;
    		}
    	} else {
    		scanner.close();
    		previous = current;
   		    current = null;
    		eof = true;
    		return null;
    	}
    }
    
    public T nextItem() {
        if (eof) return null;
    	
    	T nextItem = findNextNonNullItem();
    	
    	if (nextItem == null) return null;
    	
    	while (!comparator.compare(nextItem, current) && nextItem != null) {
    		System.err.println("Строка " + nextItem +
    				" не отсортирована и будет пропущена. Файл " + fileName);
    		
    		nextItem = findNextNonNullItem();
    	}
    	
    	if (nextItem == null) return null;
    	
    	previous = current;
    	current = nextItem;
    	return current;
    }
    
    private T findNextNonNullItem() {
    	T nextItem = getNextItem();
    	while (nextItem == null) {
            if (eof) {
                return null;
            }
            nextItem = getNextItem();
        }
        return nextItem;
    }
    
    public T getPrevious() {
		return previous;
	}

	public T getCurrent() {
		return current;
	}
}
