package mergefiles.merge;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import mergefiles.cmdline.AppSettings;
import mergefiles.cmdline.SortMode;
import mergefiles.comparators.SortingComparator;
import mergefiles.deserializer.Deserializer;

public class FilesMerger<T extends Comparable<T>> {
	private final AppSettings settings;
	private final Deserializer<T> deserializer;
	private SortingComparator<T> comparator;
	private Map<T, FileScannerPair<T>> currentValues = new HashMap<>();
	
	public FilesMerger(AppSettings settings, Deserializer<T> deserializer) {
		this.settings = settings;
		this.deserializer = deserializer;
		
		setComparator();
	}
	
	public void mergeSortedFiles() {
		for (File file : settings.getInput()) {
			FileScannerPair<T> pair = new FileScannerPair<>(file.getAbsolutePath(), 
					                                        deserializer,
					                                        comparator);
			T line = pair.getCurrent();
			if (line != null) {
				currentValues.put(line, pair);
			}
		}
		
		try (PrintWriter printWriter = new PrintWriter(settings.getOutput())) {
			while (!currentValues.isEmpty()) {
				Map.Entry<T, FileScannerPair<T>> currPair = getNextPair();
				
				if (currPair != null) {
					T currLine = currPair.getKey();
					
					T nextLine = currPair.getValue().nextItem();
					if (nextLine != null) {
						currentValues.put(nextLine, currPair.getValue());
						printWriter.println(currLine);
					} else {
						currentValues.put(currLine, currPair.getValue());
						printWriter.println(currLine);
					}
				}
				
				currentValues.remove(currPair.getKey());
			}
		} catch (IOException e) {
	    	System.err.println(e.getMessage());
	    }
	}
	
	private Map.Entry<T, FileScannerPair<T>> getNextPair() {
		Map.Entry<T, FileScannerPair<T>> mine = null;
		for (Map.Entry<T, FileScannerPair<T>> theirs : currentValues.entrySet()) {
			if (mine == null || comparator.compare(mine.getKey(), theirs.getKey())) {
                mine = theirs;
            }
		}
		
		return mine;
	}

	private void setComparator() {
		this.comparator = (a, b) -> {
			
			int	res = a.compareTo(b);
			
			if (settings.getSortMode() == SortMode.DESCEND) {
				return (res < 0);
			} else {
				return (res >= 0);
			}
		};
	}
}
